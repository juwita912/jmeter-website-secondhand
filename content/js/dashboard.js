/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 92.79899812147777, "KoPercent": 7.201001878522229};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.3298371947401378, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.44416243654822335, 500, 1500, "GET Offers by User ID-1"], "isController": false}, {"data": [0.0, 500, 1500, "PUT Profile"], "isController": false}, {"data": [0.5228426395939086, 500, 1500, "GET Offers by User ID-0"], "isController": false}, {"data": [0.4975, 500, 1500, "GET Profile"], "isController": false}, {"data": [0.4925, 500, 1500, "GET Categories by ID"], "isController": false}, {"data": [0.4575, 500, 1500, "POST Product"], "isController": false}, {"data": [0.1325, 500, 1500, "POST Registration"], "isController": false}, {"data": [0.34, 500, 1500, "GET Categories"], "isController": false}, {"data": [0.4025, 500, 1500, "Put Product ID"], "isController": false}, {"data": [0.445, 500, 1500, "POST Offers"], "isController": false}, {"data": [0.0625, 500, 1500, "POST Login"], "isController": false}, {"data": [0.0175, 500, 1500, "GET Offers by User ID"], "isController": false}, {"data": [0.5175, 500, 1500, "DELETE Product by ID"], "isController": false}, {"data": [0.51, 500, 1500, "GET Product by ID"], "isController": false}, {"data": [0.44, 500, 1500, "GET Product"], "isController": false}, {"data": [0.0, 500, 1500, "PUT Offer"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3194, 230, 7.201001878522229, 1285.8619286161563, 0, 5823, 1125.5, 2326.0, 2723.25, 3482.450000000009, 17.096213033587585, 65.89041905024756, 451.61894800448283], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["GET Offers by User ID-1", 197, 0, 0.0, 1203.2944162436543, 419, 2265, 1172.0, 1641.8000000000002, 1806.4999999999998, 2143.4800000000014, 1.1412086384280287, 16.535780982076652, 0.4591581631175271], "isController": false}, {"data": ["PUT Profile", 200, 3, 1.5, 2836.795, 511, 5823, 2817.0, 3654.3, 4209.949999999999, 5161.600000000003, 1.1181742450926129, 1.454811302854699, 116.73408252754902], "isController": false}, {"data": ["GET Offers by User ID-0", 197, 0, 0.0, 898.1015228426396, 264, 2346, 867.0, 1249.2, 1367.199999999999, 2282.3000000000006, 1.1377155595596984, 1.2254998169981635, 0.4910842552005729], "isController": false}, {"data": ["GET Profile", 200, 3, 1.5, 975.875, 28, 2419, 824.5, 1816.7, 2067.7499999999995, 2334.7000000000003, 1.1927480916030535, 1.551661600593392, 0.4911233450620229], "isController": false}, {"data": ["GET Categories by ID", 200, 0, 0.0, 1026.5299999999997, 131, 2671, 956.5, 1642.6000000000001, 1867.6499999999999, 2664.2200000000007, 1.1546610781070485, 0.8256841547303577, 0.5013754900092949], "isController": false}, {"data": ["POST Product", 200, 3, 1.5, 1066.525, 174, 2375, 1029.5, 1437.8000000000002, 1603.6499999999999, 1861.7000000000003, 1.160914336131137, 1.8912473589198855, 121.42374896968853], "isController": false}, {"data": ["POST Registration", 200, 3, 1.5, 1891.3750000000005, 798, 3075, 1865.5, 2603.6, 2684.8, 3044.99, 1.1176556056016898, 1.8997744082013568, 0.3274381657036201], "isController": false}, {"data": ["GET Categories", 200, 0, 0.0, 1382.02, 184, 3843, 1277.0, 2317.2000000000003, 2515.95, 3745.9, 1.1368610130568486, 1.187930941377762, 0.47033183551894864], "isController": false}, {"data": ["Put Product ID", 200, 3, 1.5, 1194.0849999999991, 258, 2754, 1123.0, 1730.6, 1940.1499999999999, 2587.0400000000027, 1.184427151808028, 1.9211431535669024, 245.9034146387053], "isController": false}, {"data": ["POST Offers", 200, 3, 1.5, 1075.755, 256, 2758, 1024.0, 1529.5, 1881.5499999999986, 2575.430000000002, 1.1568985862699275, 3.8720740407864596, 0.5995084627131585], "isController": false}, {"data": ["POST Login", 200, 3, 1.5, 2022.6050000000007, 784, 3341, 1961.5, 2842.4, 3141.85, 3277.92, 1.114647019155209, 1.8902247285137854, 0.31118680648055774], "isController": false}, {"data": ["GET Offers by User ID", 200, 3, 1.5, 2086.3599999999997, 727, 4611, 2088.0, 2439.0, 2675.4499999999994, 3853.5300000000016, 1.152252942565952, 17.675256322267863, 0.9502260936320741], "isController": false}, {"data": ["DELETE Product by ID", 200, 3, 1.5, 857.9950000000002, 32, 2255, 760.5, 1630.2000000000003, 1812.4499999999991, 2235.850000000002, 1.1908731481922545, 1.0136726192957177, 0.5228270379561044], "isController": false}, {"data": ["GET Product by ID", 200, 3, 1.5, 907.6799999999996, 36, 1899, 892.5, 1257.0, 1375.6999999999998, 1894.850000000001, 1.1782795939648518, 1.841919110001237, 0.4919835103452948], "isController": false}, {"data": ["GET Product", 200, 0, 0.0, 1141.740000000001, 197, 2248, 1098.5, 1546.9, 1722.9499999999998, 2127.4400000000023, 1.172037528641667, 15.228819267417943, 0.48259560896139897], "isController": false}, {"data": ["PUT Offer", 200, 200, 100.0, 0.0, 0, 0, 0.0, 0.0, 0.0, 0.0, 1.1620880397898945, 1.2937308255473434, 0.0], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Non HTTP response code: java.net.URISyntaxException/Non HTTP response message: Illegal character in path at index 44: https://secondhand.binaracademy.org/offers/${id}.json", 200, 86.95652173913044, 6.261740763932373], "isController": false}, {"data": ["401/Unauthorized", 21, 9.130434782608695, 0.6574827802128992], "isController": false}, {"data": ["404/Not Found", 9, 3.9130434782608696, 0.2817783343769568], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3194, 230, "Non HTTP response code: java.net.URISyntaxException/Non HTTP response message: Illegal character in path at index 44: https://secondhand.binaracademy.org/offers/${id}.json", 200, "401/Unauthorized", 21, "404/Not Found", 9, "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": ["PUT Profile", 200, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["GET Profile", 200, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["POST Product", 200, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST Registration", 200, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["Put Product ID", 200, 3, "404/Not Found", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST Offers", 200, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST Login", 200, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["GET Offers by User ID", 200, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["DELETE Product by ID", 200, 3, "404/Not Found", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["GET Product by ID", 200, 3, "404/Not Found", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["PUT Offer", 200, 200, "Non HTTP response code: java.net.URISyntaxException/Non HTTP response message: Illegal character in path at index 44: https://secondhand.binaracademy.org/offers/${id}.json", 200, "", "", "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
